# Read Firefox Bookmarks

Firefox profiles have caused me too much grief for polite words to express. This tool wrangles firefox's bookmark system to sort bookmarks by their tags, to then be interpreted by another tool as necessary, e.g. for display on a custom homepage.

## Usage

Usage is as follows:
`read-bookmarks.py BOOKMARKS_SRC PARENT_FOLDER N_TAGS N_BOOKMARKS`
where

- BOOKMARKS_SRC is the bookmarks.sqlite file, usually located under profile/weave/
- PARENT_FOLDER is the folder which contains your bookmarks; cannot be "Other Bookmarks"
- N_TAGS is the max number of tags to list bookmarks for
- N_BOOKMARKS is the max number of bookmarks to list for each tag
  bookmarks are outputted in the following format:

```
tag1
bookmarkName::bookmarkUrl
bookmarkName::bookmarkUrl
---
tag2
bookmarkName::bookmarkUrl
bookmarkName::bookmarkUrl
---
tag3
bookmarkName::bookmarkUrl
bookmarkName::bookmarkUrl
```

where tags are sorted by number of associated bookmarks, and bookmarks are sorted by most recently added.

## Requirements

Requires pandas to be installed
