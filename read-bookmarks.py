import os
import sqlite3
import argparse
import pandas as pd


def main():
    # Parse args
    args = get_args()

    # Load bookmarks file
    bookmarks = read_bookmarks(args.bookmarks_src, args.parent_folder)

    # Organize bookmarks
    bookmarks_by_tag, tag_order = organize_bookmarks(bookmarks)

    # Output bookmakrs
    output_bookmarks(bookmarks_by_tag, tag_order, args.n_tags, args.n_bookmarks)


def get_args():
    parser = argparse.ArgumentParser(
        prog="read-bookmarks.py", description="Read and parse firefox bookmarks"
    )
    parser.add_argument("bookmarks_src")
    parser.add_argument("parent_folder")
    parser.add_argument("n_tags")
    parser.add_argument("n_bookmarks")
    return parser.parse_args()


def read_bookmarks(bookmarks_src, parent_folder):
    """Read bookmark titles, dates, urls and tags in parent_folder"""
    # NOTE: bookmark_src is usually in profile/weave/bookmarks.sqlite
    conn = sqlite3.connect(bookmarks_src)
    cur = conn.cursor()

    # Read all bookmark in the right folder
    cur.execute("SELECT guid FROM items WHERE title='%s'" % parent_folder)
    parent_guid = cur.fetchone()[0]
    bookmarks = pd.read_sql_query(
        """SELECT id, title, urlId, dateAdded
        FROM items
        WHERE parentGuid='%s'"""
        % parent_guid,
        conn,
    )

    # Read the urls of the bookmarks
    urls = []
    for url_id in bookmarks["urlId"]:
        cur.execute("SELECT url FROM urls WHERE id='%s'" % url_id)
        url = cur.fetchone()[0]
        urls.append(url)
    bookmarks["url"] = urls

    # Read the tags of the bookmarks
    tags = []
    for bookmark_id in bookmarks["id"]:
        cur.execute("SELECT tag FROM tags WHERE itemId='%s'" % bookmark_id)
        bookmark_tags = [row[0] for row in cur.fetchall()]
        tags.append(bookmark_tags)
    bookmarks["tags"] = tags

    return bookmarks[["title", "url", "tags", "dateAdded"]]


def organize_bookmarks(bookmarks):
    """Sort bookmarks into a dict by tag, and then by date created"""
    # Sort tags by most used
    tags = []
    for bk_tags in bookmarks["tags"]:
        tags += bk_tags
    tag_set = list(set(tags))
    tag_count = [tags.count(t) for t in tag_set]
    tags_sorted = sorted(
        tag_set, key=lambda t: tag_count[tag_set.index(t)], reverse=True
    )

    # Organize bookmarks by tag and date added
    organized_bookmarks = {}
    for tag in tags_sorted:
        tagged_idx = [i for i in range(len(bookmarks)) if tag in bookmarks["tags"][i]]
        tagged_bookmarks = bookmarks.iloc[tagged_idx]
        tagged_bookmarks = tagged_bookmarks.sort_values(
            by="dateAdded", axis=0, ascending=False
        )
        organized_bookmarks[tag] = list(
            zip(list(tagged_bookmarks["title"]), list(tagged_bookmarks["url"]))
        )

    # Because the dictionary is unsorted, return the order seperately
    return organized_bookmarks, tags_sorted


def output_bookmarks(bookmarks_by_tag, tags_sorted, n_tags, n_bookmarks):
    """Print as many tags and associated bookmarks as required,
    In an easy-to-parse way"""
    output_string = ""
    for t in range(min(n_tags, len(tags_sorted))):
        output_string += "---\n" if t == 0 else ""
        tag = tags_sorted[t]
        output_string += tag + "\n"
        for b in range(min(n_bookmarks, len(bookmarks_by_tag[tag]))):
            bk = bookmarks_by_tag[tag][b]
            title = bk[0]
            url = bk[1]
            output_string += "%s::%s\n" % (title, url)

    print(output_string)


if __name__ == "__main__":
    main()
